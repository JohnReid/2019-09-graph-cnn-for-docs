\documentclass[xcolor={usenames, dvipsnames, svgnames, table}]{beamer}

%
% Customise beamer
\usetheme{Pittsburgh}
\usecolortheme[snowy]{owl}  % Dark on light
\setbeamertemplate{footline}{}  % Completely remove footline
\setbeamertemplate{frametitle continuation}{[\insertcontinuationcount]}  % Nicer numbering
%
% Uncomment line below to make printable notes
% \setbeameroption{show only notes}
%
% Remove icons from bibliography
% https://tex.stackexchange.com/a/53131/39498
\setbeamertemplate{bibliography item}{}


%
% PDF metadata
\usepackage{hyperref}
\hypersetup{%
  pdfauthor={John Reid},
  % pdftitle={<TITLE>},
  % pdfkeywords={<KEYWORDS>},
  pdfcreator={xelatex},
  % pdfproducer={xelatex},
}


%
% Math
\usepackage{amsmath}  % For general maths
\usepackage{pifont}  % http://ctan.org/pkg/pifont
\usepackage{bm}  % For bold math
\usepackage{esdiff}  % For derivatives
\usepackage{mathtools}
\usepackage{commath}
\usepackage{siunitx}
\usepackage{newtxmath}
%\usepackage{amsthm}
%\usepackage{thmtools}
%\usepackage{wasysym}
%\usepackage{amssymb} % not with newtxmath
%\usepackage{mathabx} % not with newtxmath
\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}


%
% Miscellaneous
%\usepackage{hanging}  % Hanging paragraphs
\usepackage{booktabs}  % Nice tables
% \usepackage{layouts}   % Info about layouts
\usepackage{graphicx}  % Graphics
\usepackage[normalem]{ulem} % Underlining
% \usepackage{minted}  % For syntax highlighting source code
\usepackage{setspace}  % For setstretch
\usepackage{calc}  % For widthof()
\usepackage{fontawesome}  % For Twitter symbol, etc...
\usepackage{silence}  % To silence some warning
% \WarningFilter{biblatex}{Patching footnotes failed}
\usepackage{filecontents}


%
% TikZ
\usepackage{tikz}
\pgfdeclarelayer{background}
\pgfdeclarelayer{foreground}
\pgfsetlayers{background, main, foreground}
\usetikzlibrary{
  arrows,
  arrows.meta,
  shapes,
  decorations.pathmorphing,
  backgrounds,
  positioning,
  fit,
  shadows,
  calc,
  graphs,
  graphs.standard,
  bayesnet}


%
% Set up biblatex
\usepackage[
  doi=false,
  isbn=false,
  url=false,
  sorting=none,
  style=authoryear,
  backend=biber]{biblatex}
%
% We specify the database.
% \addbibresource{2019-06-PatchMatch-BP.bib}  % chktex-file 8
\DeclareCiteCommand{\citejournal}
  {\usebibmacro{prenote}}
  {\usebibmacro{citeindex}%
   \usebibmacro{cite}
   \usebibmacro{journal}}
  {\multicitedelim}
  {\usebibmacro{postnote}}
%
% To cite in frame footnote
\newcommand{\myfootcite}[1]{\footnote[frame]{\cite{#1}}}
%
% A footnote without a marker from
% https://tex.stackexchange.com/questions/30720/footnote-without-a-marker/30726#30726
\newcommand\blfootnote[1]{
  \begingroup
    \renewcommand\thefootnote{}\footnote[frame]{#1}
    \addtocounter{footnote}{-1}
  \endgroup
}


%
% Fonts
%
\usepackage{fix-cm}  % just to avoid some spurious messages
\usefonttheme{professionalfonts}  % using non standard fonts for beamer
%\usefonttheme{serif}  % default family is serif
\setmainfont{TeX Gyre Bonum}


%
% Quotations
% See: https://tex.stackexchange.com/questions/365231/enclose-a-custom-quote-environment-in-quotes-from-csquotes
%
\def\signed#1{{\leavevmode\unskip\nobreak\hfil\penalty50\hskip1em
  \hbox{}\nobreak\hfill #1%
  \parfillskip=0pt \finalhyphendemerits=0 \endgraf}}

  \newsavebox\mybox{}
\newenvironment{aquote}[1]
  {\savebox\mybox{#1}\begin{quote}\openautoquote\hspace*{-.7ex}}
  {\unskip\closeautoquote\vspace*{1mm}\signed{\usebox\mybox}\end{quote}}


%
% COMMANDS
%
% Todo
\newcommand{\todo}[1]{\textbf{TODO}: #1}
\newcommand{\hide}[1]{}
%
% Cpp command
\newcommand{\cpp}{C\nolinebreak\hspace{-.05em}\raisebox{.4ex}{\tiny\bf +}\nolinebreak\hspace{-.10em}\raisebox{.4ex}{\tiny\bf +}}
%
% Tick and cross
\newcommand{\cmark}{\ding{51}}
\newcommand{\xmark}{\ding{55}}


%
% Document metadata
\title{Graph neural networks for document understanding}
% \subtitle{How modern networks don't know what they don't know}
% \date{February 19, 2018}
\author{John Reid \& Thomas Sajot}
\institute{Blue Prism}
%
% Fix from https://tex.stackexchange.com/a/444420/39498
\def\titlepage{%
  \usebeamertemplate{title page}
}


%
% Document
\begin{document}

\maketitle


\begin{frame}{Visually rich documents}
  \begin{center}
    \includegraphics[width=\textwidth]{figures/VRD}
  \end{center}
\end{frame}


\begin{frame}{The graph representation}
  \begin{center}
    \includegraphics[width=\textwidth]{figures/graph}
  \end{center}
  Node embedding: $\mathbf{t}_i = \textnormal{Bi-LSTM}(x_1, \dots, x_m)$ \\
  Edge embedding: $\mathbf{r}_{ij} = [x_{ij}, y_{ij}, \frac{w_i}{h_i}, \frac{h_j}{h_i}, \frac{w_j}{h_i}]$
  \\
  \vspace{15pt}
  NB\@: word level prediction task!

\end{frame}


\begin{frame}{Graph convolution}
  \begin{center}
    \includegraphics[width=\textwidth]{figures/convolution}
  \end{center}
\end{frame}


\begin{frame}{Attention}
  \begin{center}
    \includegraphics[width=\textwidth]{figures/attention}
  \end{center}
\end{frame}


\begin{frame}{Attention}
  \begin{center}
    \begin{tabular}{ccc}
      & Graph Attention Networks & this paper \\
      \toprule
        & $h_i = W t_i $
        & $h_{ij} = \textnormal{MLP} (t_i \parallel{} r_{ij} \parallel{} t_j)$ \\
      $e_{ij}$
        & $\textnormal{LeakyReLU} (a^T [h_i \parallel{} h_j])$
        & $\textnormal{LeakyReLU} (w_a^T h_{ij})$ \\
      $t_i'$
        & $\sigma \big( \sum_{j \in \mathcal{N}_i} \alpha_{ij} h_j \big)$
        & $\sigma \big( \sum_{j=1}^N \alpha_{ij} h_{ij} \big)$ \\
      $r_{ij}'$
        &
        & $\textnormal{MLP}(h_{ij})$
    \end{tabular}
  \end{center}
  \vspace{10pt}
  where $\alpha_{ij} = \textnormal{softmax}_j(e_{ij})$
\end{frame}


\begin{frame}{Conditional random field: CRF}
  \begin{center}
    \includegraphics[width=\textwidth]{figures/CRF}
  \end{center}
  \begin{align*}
    p(Y|X) \propto \exp \sum_{i=2}^n \sum_{j=1}^J \lambda_j f_j(Y_{i-1}, Y_i, i, X)
  \end{align*}
\end{frame}


\begin{frame}{BiLSTM-CRF}
  \begin{center}
    \includegraphics[width=\textwidth]{figures/BiLSTM-CRF}
  \end{center}
\end{frame}


\begin{frame}{Text segment BiLSTM-CRF}
  \begin{center}
    \includegraphics[width=\textwidth]{figures/BiLSTM-embeddings}
  \end{center}
\end{frame}


\begin{frame}{Multi-task learning: auxiliary loss}
  \begin{itemize}
    \item Also label and predict each text segment
    \item Use as auxiliary task
    \item Decreases training time
    \item Improves performance
  \end{itemize}
\end{frame}


\begin{frame}{Data}
  \begin{columns}
    \column[t]{.5\textwidth}
      \begin{block}{VATI}
        \begin{itemize}
          \item Chinese tax invoices
          \item 3,000 pictures
          \item 16 fields to extract
          \item fixed template
          \item distracting objects
          \item potentially skewed
        \end{itemize}
      \end{block}
      \column[t]{.5\textwidth}
      \begin{block}{IPR}
        \begin{itemize}
          \item 1,500 receipts
          \item fields:
            \begin{itemize}
              \item invoice number
              \item payer name
              \item vendor name
              \item total amount
            \end{itemize}
          \item 146 templates
        \end{itemize}
      \end{block}
  \end{columns}
\end{frame}


\begin{frame}{Results}
  \begin{center}
    \includegraphics[scale=.25]{figures/results}
  \end{center}
\end{frame}


\begin{frame}{Ablation study}
  \begin{center}
    \includegraphics[scale=.25]{figures/ablation}
  \end{center}
\end{frame}


\begin{frame}{Results II}
  \begin{center}
    \includegraphics[scale=.2]{figures/results-2}
  \end{center}
\end{frame}

% \section{References}
% \begin{frame}[allowframebreaks]{References}
%   % \renewcommand*{\bibfont}{\scriptsize}
%   \renewcommand*{\bibfont}{\footnotesize}
%   \printbibliography[heading=none]{}
% \end{frame}

\end{document}
